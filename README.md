# Wardrobify

Team:

* Myo - shoes microservice
* Jack - hats microservice

## Design

## Shoes microservice



Explain your models and integration with the wardrobe
microservice, here.

Create the models for shoe microservice
2 models:
- Shoe model contains:
    manufacturer
    model name
    color
    URL for a picture
    bin (foreign key)

-BinVO model contains:
    closet_name
    bin_number
    bin_size

## Hats microservice

create a model for the hats, another for polling the wardrobe
hat model contains fabric, style name, color, picture url, location(foreign key to location model)

location model contains closet name, section number, shelf number 

Explain your models and integration with the wardrobe
microservice, here.
