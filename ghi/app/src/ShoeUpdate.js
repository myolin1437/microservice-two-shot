import React from "react";

class ShoesUpdate extends React.Component {
    constructor(props) {
        console.log(props)
        super(props)
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            pictureUrl: '',
            bin: '',
            bins: [],
        };
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }  

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.picture_url = data.pictureUrl;
        delete data.modelName;
        delete data.pictureUrl;
        delete data.bins;
        console.log(data);
        
        const binUrl = `http://localhost:8080/api/shoes/${data.id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                pictureUrl: '',
                bin: '',
            };
            this.setState(cleared);
        }
    }
    
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
      }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({modelName: value})
      }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
        }
      
    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value})
      }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value})
      }

    async componentDidMount() {
        const detailUrl = `http://localhost:8080/api/shoes/15/`;
        const binUrl = 'http://localhost:8100/api/bins/'
    
        const detailResponse = await fetch(detailUrl);
        const binResponse = await fetch(binUrl)
    
        if (detailResponse.ok && binResponse.ok) {
          const detailData = await detailResponse.json();
          const binData = await binResponse.json();
          this.setState({
            manufacturer: detailData.manufacturer,
            modelName: detailData.model_name,
            color: detailData.color,
            pictureUrl: detailData.picture_url,
            bin: detailData.bin.id,
            bins: binData.bins,
        });
        }
      }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Update shoes</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.modelName} onChange={this.handleModelNameChange} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control"/>
                    <label htmlFor="model_name">Model Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.pictureUrl} onChange={this.handlePictureUrlChange} placeholder="Picture url" type="text" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture Url</label>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.bin} onChange={this.handleBinChange} required name="bin" id="bin" className="form-select">
                      {/* <option value="">Choose a bin</option> */}
                      {this.state.bins.map(bin => {
                            return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                            );
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Update</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    
}

export default ShoesUpdate;