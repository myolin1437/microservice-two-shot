import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatList';
import HatForm from './HatForm';
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm';
import ShoesUpdate from './ShoeUpdate';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
            <Route path="update" element={<ShoesUpdate />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
